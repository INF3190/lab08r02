/**
 * @author Johnny Tsheke
 */

$(document).ready(function() {
    $("#etudiants").DataTable({
                        "processing":true,
                        ajax:"js/etudiants.json",
                        columns:[
                        {data:"nom"},
                        {data:"prenom"},
                        { data: "codeperm"},
                        {data:"note"}
                        ]
                    
                });
} );