/**
 * @author Johnny Tsheke
 */
$(document).ready(function () {

    $('#locaux').jstree({
        'core': {
            'data': {
                'url': function (node) {
                    return node.id === '#' ?
                        'js/locaux.json' :
                        'js/locaux.json';
                },
                'data': function (node) {
                    return { 'id': node.id };
                },
                'dataType': 'json'
            }
        }
    });
});